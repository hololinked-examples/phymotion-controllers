# -*- coding: utf-8 -*-

import setuptools

long_description="""
This library adds high level HTTP support to interface with Phytron Phymotion
controllers in Python via USB or LAN. Maintains reasonable compliance with WoT standards. 
"""

setuptools.setup(
    name="phymotion-controllers",
    version="1.1",
    author="Vignesh Vaidyanathan",
    author_email="vignesh.vaidyanathan@hololinked.dev",
    description="Phytron Phymotion interfacing library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=["phymotion"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires = [
        'pyserial',
    ],
    python_requires='>=3',
)