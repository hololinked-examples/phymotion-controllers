# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 00:06:16 2020
@author: Leonard.Doyle
https://gitlab.lrz.de/cala-public/packages/pyphymotion
Adapted from Datenwolf - https://github.com/datenwolf

adapted second time by: Vignesh Vaidyanathan, 2024 for use with WoT applications
"""
import asyncio
import datetime
import typing
from hololinked.server import Thing, action, HTTP_METHODS, Event
from hololinked.server.properties import Integer, Selector, Number, Boolean, ClassSelector

from .commcontrol import CommControl
from .constants import MovementTypes, PositionUnitTypes, LimitSwitchTypes
from .constants import StepResolutions, AxStatusBits
from .schema import move_relative_schema, define_position_schema, PositionHistory



class Axis(Thing):
    """
    Represents a single stepper module of a Phytron Phymotion Control Rack
    """


    drive_ID = Integer(default=1, bounds=(1, 16), URL_path='/module/id', 
                        doc="module number within the controller stack (1-16)")
    
    axis = Integer(default=1, bounds=(1, 4), URL_path='/channel', 
                        doc="Axis on module (1-4), always 1 for single axis I1AM01 modules")
    
    comm_handle = ClassSelector(default=None, allow_None=True, 
                            class_=CommControl, remote=False, doc="serial communication handle")

    def __init__(self, instance_name : str, comm_handle : CommControl, drive_ID : int, 
                axis : int = 1, **kwargs) -> None:
        """
        Parameters
        ----------
        instance_name: str,
            instance name of the module
        comm_handle: 
            a instance of PyPhytron.CommControl class, encapsulating a communication bus
        drive_ID: int
            module number within the controller stack (1-16)
        axis: int
            Axis on module (1-4), always 1 for single axis I1AM01 modules
        """
        super().__init__(instance_name=instance_name, comm_handle=comm_handle, drive_ID=drive_ID, 
                        axis=axis, **kwargs)
        asyncio.get_event_loop().call_soon(lambda : asyncio.create_task(self.monitor_position()))

    def directcmd(self, cmdstr):
        """Convenience function to execute direct global command without ax prefix."""
        return self.comm_handle.execute(cmdstr)

    def execute(self, cmdstr):
        return self.comm_handle.executeOnAxis(self.drive_ID, self.axis, cmdstr)


    #********Attributes/Parameters *****************
    AllowedStates = [0,1,3]
    """
    equals 0: reboot occurred 
    equals 1: init_device has been called or Enable has been called and position will be known only 
            if HW referenced.  
    equals 3: PositionOverride was used, so the user actively decided to override the reference. 
    """

    @property
    def State(self):
        return int(self.directcmd('R{:d}R'.format(self.drive_ID)))

    @State.setter
    def State(self, value):
        if value in self.AllowedStates:
            self.directcmd('R{:d}={:d}'.format(self.drive_ID, value))
        else:
            raise ValueError("Currently state can be only {}. Given value : {}.".format(self.AllowedStates, value))


    def get_movement_type(self):
        resp = self.execute('P01R')
        return MovementTypes(int(resp))

    def set_movement_type(self, value):
        self.execute('P01S{}'.format(value))

    movement_type = Selector(default=0, objects=[0, 1, 2, 3], URL_path='/movement-type', db_persist=True, 
                        fget=get_movement_type, fset=set_movement_type, 
                        doc="""Type of movement (free run, relative / absolute, reference run)
                            0 = ignore limit switches, for example with rotational movement,
                            1 = use hardware limit switches, for example, with XY tables or other
                                linear systems, 2 limit switches: limit direction minus, Limit direction plus,
                            2 = software limit switches are monitored,
                            3 = both hardware and software limit switches are monitored
                            """
                    )

    def get_position_units(self):
        resp = self.execute('P02R')
        return PositionUnitTypes(int(resp))

    def set_position_units(self, value):
        # accepts ints or PositionUnitTypes, they both evaluate to int in {}.format
        self.execute('P02S{}'.format(value))

    position_units = Selector(default=1, objects=[1, 2, 3, 4], URL_path='/position-units', db_persist=True,
                            fget=get_position_units, fset=set_position_units, 
                            doc="""Measuring units of movement: used by hardware but only for displaying
                                1 = step, 2 = mm, 3 = inch, 4 = degree""")

    
    def get_conversion_factor(self):
        resp = self.execute('P03R')
        return float(resp)

    def set_conversion_factor(self, value):
        self.execute('P03S{:f}'.format(value))

    conversion_factor = Number(bounds=(0, None), inclusive_bounds=(False, True), URL_path='/conversion-factor',
                            fget=get_conversion_factor, fset=set_conversion_factor, db_persist=True,
                            doc="""The conversion factor, distance=conversion*steps, steps=distance/conversion.
                                Example: 1mm travel per 400steps: conversion=1/400=0.0025, Caution: When changing 
                                the step resolution e.g. from 1/4 to 1/2, this factor also has to change.
                                """
                        )
    
    
    def get_start_stop_frequency(self):
        resp = self.execute('P04R')
        return int(resp)

    def set_start_stop_frequency(self, value):
        self.execute('P04S{}'.format(value))

    start_stop_frequency = Integer(bounds=(0, None), inclusive_bounds=(False, True), step=100,  db_persist=True,
                                URL_path='/frequencies/start-stop', fget=get_start_stop_frequency, fset=set_start_stop_frequency, 
                                doc="""Start and Stop frequency in Hz with which to start the movement. Default = 400"""
                            )
    
    #P05 reserved, unused
    #P06 reserved, unused

    def get_emergency_stop_ramp(self):
        resp = self.execute('P07R')
        return int(resp)

    def set_emergency_stop_ramp(self, value):
        self.execute('P07S{}'.format(value))

    emergency_stop_ramp = Number(bounds=(4000, None), inclusive_bounds=(False, True), step=4000, 
                                fget=get_emergency_stop_ramp, fset=set_emergency_stop_ramp,
                                db_persist=True, URL_path='/frequencies/emergency-stop-ramp',
                                doc="Emergency stop ramp, Input for I1AM0x: in 4000 Hz/s steps,  I4XM01: in 1 Hz/s steps, Default = 100 000"
                            )


    def get_referencing_run_frequency(self):
        resp = self.execute('P08R')
        return int(resp)

    def set_referencing_run_frequency(self, value):
        self.execute('P08S{}'.format(value))

    referencing_run_frequency = Number(bounds=(0, 40000), inclusive_bounds=(False, True), step=100,
                                    URL_path='/frequencies/referencing-run', db_persist=True,
                                    fget=get_referencing_run_frequency, fset=set_referencing_run_frequency,
                                    doc="""Run frequency during initializing (referencing), in Hz (integer value).
                                        I1AM0x: 40 000 maximum, I4XM01: 4 000 000 maximum""" 
                                )


    def get_referencing_acceleration(self):
        resp = self.execute('P09R')
        return int(resp)

    def set_referencing_acceleration(self, value):
        self.execute('P09S{}'.format(value))

    referencing_acceleration_frequency = Number(bounds=(0, 40000), inclusive_bounds=(False, True), step=100,
                                    URL_path='/frequencies/referencing-acceleration', db_persist=True,
                                    fget=get_referencing_acceleration, fset=set_referencing_acceleration,
                                    doc="""Ramp during initializing, associated to parameter P08 (ReferencingRunFrequency).
                                        Input for I1AM0x: in 4000 Hz/s steps, I4XM01: in 1 Hz/s steps, Default = 4000""" 
                                )


    def get_referencing_leave_frequency(self):
        resp = self.execute('P10R')
        return int(resp)

    def set_referencing_leave_frequency(self, value):
        self.execute('P10S{}'.format(value))

    referencing_leave_frequency = Number(bounds=(0, 40000), inclusive_bounds=(False, True), step=100, 
                                        URL_path='/frequencies/referencing-leave', db_persist=True,
                                        fget=get_referencing_leave_frequency, fset=set_referencing_leave_frequency,
                                        doc="""Run frequency for leaving the limit switch
                                            in Hz (integer value), Default = 400"""
                                        )


    def get_referencing_limit_plus_offset(self):
        resp = self.execute('P11R')
        return float(resp)

    def set_referencing_limit_plus_offset(self, value):
        self.execute('P11S{}'.format(value))

    referencing_limit_plus_offset = Number(bounds=(0, None), URL_path='/referencing-limits/plus-offset', 
                                        db_persist=True, fget=get_referencing_limit_plus_offset, 
                                        fset=set_referencing_limit_plus_offset,
                                        doc="""offset for limit switch direction + (away from 'LIMIT+' switch,
                                                                            towards 'LIMIT–' switch)
                                        Distance between reference point M0P and limit switch activation
                                        Unit: as defined in parameter P02 (PositionUnits)
                                        Value >= 0, Default = 0"""
                                    )


    def get_referencing_limit_minus_offset(self):
        resp = self.execute('P12R')
        return float(resp)

    def set_referencing_limit_minus_offset(self, value):
        self.execute('P12S{}'.format(value))

    referencing_limit_minus_offset = Number(bounds=(0, None), URL_path='/referencing-limits/minus-offset', 
                                        db_persist=True, fget=get_referencing_limit_minus_offset, 
                                        fset=set_referencing_limit_minus_offset,
                                        doc="""offset for limit switch direction + (away from 'LIMIT+' switch,
                                                                            towards 'LIMIT–' switch)
                                        Distance between reference point M0P and limit switch activation
                                        Unit: as defined in parameter P02 (PositionUnits)
                                        Value >= 0, Default = 0"""
                                    )


    def get_referencing_recovery_time(self):
        resp = self.execute('P13R')
        return int(resp)

    def set_referencing_recovery_time(self, value):
        self.execute('P13S{}'.format(value))

    referencing_recovery_time = Integer(bounds=(0, None), URL_path='/referencing-recovery-time', db_persist=True,
                                        fget=get_referencing_recovery_time, fset=set_referencing_recovery_time,
                                        doc= "Time lapse during initialization in msec, default 20msec")


    def get_run_frequency(self):
        resp = self.execute('P14R')
        return int(resp)

    def set_run_frequency(self, value):
        self.execute('P14S{}'.format(value))

    run_frequency = Integer(bounds=(0, None), step=100, inclusive_bounds=(False, True), db_persist=True, 
                            fget=get_run_frequency, fset=set_run_frequency, 
                            doc="""Run frequency during program operation. Enter in Hz (integer value)
                                    I1AM0x: 40 000 maximum, I4XM01: 4 000 000 maximum, Default = 4000""")


    def get_acceleration(self):
        resp = self.execute('P15R')
        return int(resp)

    def set_acceleration(self, value):
        self.execute('P15S{}'.format(value))

    acceleration = Integer(bounds=(0, None), inclusive_bounds=(False, True), db_persist=True, step=100,
                           fget=get_acceleration, fset=set_acceleration,
                            doc="""Ramp for run frequency in Hz/(sec steps)
                            I1AM0x: in 4000 Hz/s steps, I4XM01: in 1 Hz/s steps, Default = 4000"""
                        )

   
    def get_position_recovery_time(self):
        resp = self.execute('P16R')
        return int(resp)

    def set_position_recovery_time(self, value):
        self.execute('P16S{}'.format(value))

    position_recovery_time = Integer(bounds=(1, None), db_persist=True, URL_path='/position/recovery_time',
                                    fget=get_position_recovery_time, fset=set_position_recovery_time,
                                    doc="Time lapse during after positioning in msec.")


    def get_boost_enable(self):
        resp = self.execute('P17R')
        return bool(resp)

    def set_boost_enable(self, value):
        self.execute('P17S{}'.format(value))

    boost_enable = Boolean(db_persist=True, URL_path='/boost-enable', fget=get_boost_enable, fset=set_boost_enable, 
                        doc="""Boost (current is defined in P42, BoostCurrent)
                            0 = off, 1 = on during motor run, 2 = on during acceleration and deceleration ramp"""
                        )


    #P18 Internally used for interpolation moves
    def get_encoder_deviation_counter(self):
        resp = self.execute('P19R')
        return float(resp)

    def set_encoder_deviation_counter(self, value):
        self.execute('P19S{:+f}'.format(value))

    encoder_deviation_counter = Number(fget=get_encoder_deviation_counter, fset=set_encoder_deviation_counter,
                                    doc="", db_persist=True, URL_path='/encoder/deviation-counter')

    
    def get_position(self):
        """
        Mechanical zero counter
        """
        resp = self.execute('P20R')
        return float(resp)

    def set_position(self, value):
        # CAUTION: do not just write to position register, but use absolute drive command
        # A+333.33, otherwise position will be updated but no move occurs
        # When in steps mode, Phymotion accepts float command, but truncates decimals
        # no rounding occurs (+1.8 is same as +1)
        self.execute('A{:+f}'.format(value)) #prints either plus or minus

    position = Number(URL_path='/position', fget=get_position, fset=set_position, db_persist=True, observable=True,
                    doc="current position, write operation does not cause movement but overwrite register value.")


    @property
    def position_register(self):
        """
        Reads the current position just like Position. However writing to
        Position will move the motor, writing directly to the PositionRegister
        will update/override the number without moving the axis!
        """
        return self.position

    @position_register.setter
    def position_register(self, value):
        self.execute('P20S{:+f}'.format(value))

    # not a remote property because writing causes movement. We model that as an action. 


    def get_absolute_counter(self):       
        resp = self.execute('P21R')
        return float(resp)

    def set_absolute_counter(self, value):
        self.execute('P21S{:+f}'.format(value))

    absolute_counter = Number(fget=get_absolute_counter, fset=set_absolute_counter, URL_path='/absolute-counter',
                        doc="""Encoder, multi turn and also for single turn. The value of P22 is extended to P21 by software.
                        The encoder counters have a fixed resolution, e.g. 10 bit (for single-turn encoders: the resolution is 
                        bits per turn), then the read value repeats. A saw tooth profile of the the numerical values is 
                        produced during a continuous motor running. This course is "straightened" by software. P20 and P21 will be
                        scaled to the same value per revolution by P3 and P39 and are therefore directly comparable, see P36.
                        """
                    )


    def get_encoder_counter(self):
        resp = self.execute('P22R')
        return float(resp)
    
    def set_encoder_counter(self, value):
        self.execute('P22S{:+f}'.format(value))

    encoder_counter = Integer(doc="""Indicates the true absolute encoder position. Is only set for A/B encoders to zero (after reset),
                            the absolute encoder remains the value.""")
    

    def get_soft_limit_max(self):
        resp = self.execute('P23R')
        return float(resp)

    def set_soft_limit_max(self, value):
        self.execute('P23S{}'.format(value))

    soft_limit_max = Number(URL_path='/software-limit/max', db_persist=True,
                        fget=get_soft_limit_max, fset=set_soft_limit_max,
                        doc="Software limit maximum, 0=no limitation")



    def get_soft_limit_min(self):
        resp = self.execute('P24R')
        return float(resp)

    def set_soft_limit_min(self, value):
        self.execute('P24S{}'.format(value))

    soft_limit_min = Number(URL_path='/software-limits/min', db_persist=True, 
                        fget=get_soft_limit_min, fset=set_soft_limit_min, 
                        doc="Software limit minimum, 0=no limitation")


    def get_play_compensation(self):
        #Note: in fact, can be float even when axis is in Steps mode, so do
        # same as phymotion and always return float
        resp = self.execute('P25R')
        return float(resp)

    def set_play_compensation(self, value):
        self.execute('P25S{:f}'.format(value))

    play_compensation = Number(bounds=(0, None), URL_path='/play-compensation', db_persist=True, 
                        fget=get_play_compensation, fset=set_play_compensation,
                        doc="""Indicates the distance, the target position in the selected direction 
                        is passed over and afterwards is started in reverse direction. 0 = compensation off.
                        """
                        )

   
    def get_encoder_data_rate(self) -> int:
        resp = self.execute('P26R')
        return int(resp)

    def set_encoder_data_rate(self, value : int) -> None:
        self.execute('P26S{}'.format(value))

    encoder_data_rate = Integer(bounds=(0, 10), URL_path='/encoder/data-rate', db_persist=True,
                                fget=get_encoder_data_rate, fset=set_encoder_data_rate,
                                doc="""
                                The data transfer rate is set by P26 (ONLY for SSI
                                encoder), by which the encoder is read. The transfer
                                rate is dependent on the length of the cable by which
                                the encoder is connected to the device. The shorter the
                                cable, the encoder can more quickly be read.
                                Data transfer rate 1 to 10 (= 100 to 1000 kHz)
                                1 = 100 kHz
                                2 = 200 kHz
                                3 = 300 kHz
                                4 = 400 kHz
                                5 = 500 kHz
                                6 = 600 kHz
                                7 = 700 kHz
                                8 = 800 kHz
                                9 = 900 kHz
                                10 = 1000 kHz
                                Default = 1 = 100kHz
                                """)


    def get_limit_switch_type(self):
        resp = self.execute('P27R')
        return LimitSwitchTypes(int(resp))

    def set_limit_switch_type(self, value):
        self.execute('P27S{}'.format(value))

    limit_switch_type = Integer(bounds=(0, 7), URL_path='/limit-switch/type', db_persist=True,
                            fget=get_limit_switch_type, fset=set_limit_switch_type,
                            doc="""Limit switch type, NCC: normally closed contact,NOC: normally open contact
                                    LIM–  Center/Ref  LIM+
                                0   NCC     NCC     NCC
                                1   NCC     NCC     NOC
                                2   NOC     NCC     NCC
                                3   NOC     NCC     NOC
                                4   NCC     NOC     NCC
                                5   NCC     NOC     NOC
                                6   NOC     NOC     NCC
                                7   NOC     NOC     NOC
                                """)


    def get_enabled(self):
        return AxStatusBits.APS_ready in self.AxStatus

    def set_enabled(self, value):
        if value:
            self.execute('MA')
        else: 
            self.execute('MD')

    enabled = Boolean(URL_path='/enabled', doc="enable or disable axis for moving", 
                    fget=get_enabled, fset=set_enabled)


    def get_enabled_at_boot(self):
        resp = self.execute('P28R')
        return bool(int(resp)) #bool('0') is True, need int('0') first!

    def set_enabled_at_boot(self, value : bool): 
        self.execute('P28={}'.format(int(value)))
        self.comm_handle.ctrlSaveParams()

    enabled_at_boot = Boolean(URL_path='/enabled-at-boot', fget=get_enabled_at_boot, fset=set_enabled_at_boot,
                            doc="""0 = Power stage is deactivated after power on, 1 = Power stage is activated after power on,
                            Default = On for directly connected modules, off for modules on separate powersupply."""
                        )


    def get_frequency_band(self):
        resp = self.execute('P30R')
        return int(resp)

    def set_frequency_band(self, value):
        self.execute('P30S{}'.format(value))

    frequency_band = Selector(default=1, objects=[0, 1], db_persist=True, URL_path='/frequency-band',
                        fget=get_frequency_band, fset=set_frequency_band, 
                        doc="""For I4XM01 only! Frequency band setting, 0 = manual, 1 = automatic,
                            Default = 1. Remark: It is recommended to work with the automatic setting mode.
                            For each run frequency (P14) and ramp (P15) the controller automatically selects suitable settings.
                            """
                        )


    def get_control_predivider(self):
        resp = self.execute('P31R')
        return int(resp)

    def set_control_predivider(self, value):
        self.execute('P31S{}'.format(value))

    control_predivider = Integer(fget=get_control_predivider, fset=set_control_predivider, db_persist=True, 
                            URL_path='/control-predivider',
                            doc="""For I4XM01 only! Frequency and ramp predivider (only if P30 = 0, manual)
                                Values = 0-9, default = 3, This parameter changes the predivider which supplies
                                the hardware (frequency generated) with a clock of 20MHz derived."""
                            )

   
    def get_acceleration_ramp_shape(self):
        resp = self.execute('P32R')
        return int(resp)

    def set_acceleration_ramp_shape(self, value):
        self.execute('P32S{}'.format(value))

    acceleration_ramp_shape = Integer(fget=get_acceleration_ramp_shape, fset=set_acceleration_ramp_shape,
                                doc="""Positioning ramp shape, 0 = s-shape, 1 = linear ramp
                                    Default = 1, Remark: The s-shape ramp can be modified with P33 parameter."""
                            )


    def get_acceleration_ramp_arc(self):
        resp = self.execute('P33R')
        return int(resp)

    def set_acceleration_ramp_arc(self, value):
        self.execute('P33S{}'.format(value))

    acceleration_ramp_arc = Integer(URL_path='/acceleration-ramp-arc', bounds=(1, 32767), crop_to_bounds=True,
                                fget=get_acceleration_ramp_arc, fset=set_acceleration_ramp_arc,
                                doc="""Arc value setting for s-shape ramp (higher is steeper)
                                Values: OMC: 1 to 8191, TMC: 1 to 32767. Default = 1"""
                                )
    

    def get_encoder_type(self):
        resp = self.execute('P34R')
        return int(resp)

    def set_encoder_type(self, value):
        self.execute('P34S{}'.format(value))

    encoder_type = Integer(bounds=(0, 13), URL_path='/encoder/bounds', db_persist=True, 
                        fget=get_encoder_type, fset=set_encoder_type,
                        doc="""Encoder type, 0 = no encoder, 1 = incremental 5.0 V, 2 = incremental 5.5 V, 
                        3 = serial interface SSI binary Code 5.0 V, 4 = serial interface SSI binary Code 5.5 V, 
                        5 = serial interface SSI Gray Code 5.0 V, 6 = serial interface SSI Gray Code 5.5 V, 
                        7 = EnDat 5 V, 8 = EnDat 5.5 V, 9 = resolver, 10 = LVDT 4-wire, 11 = LVDT 5/6-wire, 
                        12 = BiSS 5,0 V, 13 = BiSS 24,0 V, Default = 0"""
                    )


    def get_encoder_resolution(self):    
        resp = self.execute('P35R')
        return int(resp)
 
    def set_encoder_resolution(self, value):
        self.execute('P35S{}'.format(value))

    encoder_resolution = Integer(bounds=(1, 24), URL_path='/encoder/resolution', db_persist=True,
                                fget=get_encoder_resolution , fset=set_encoder_resolution,
                                doc="Encoder resolution for SSI and EnDat encoder."
                            )


    
    def get_encoder_function(self):
        resp = self.execute('P36R')
        return int(resp)

    def set_encoder_function(self, value):
        self.execute('P36S{}'.format(value))

    encoder_function = Integer(bounds=(0, 1), URL_path='/encoder/usage', db_persist=True,
                            fget=get_encoder_function, fset=set_encoder_function,
                            doc="""This parameter specifies the use of P21 as a pure counter or whether its value 
                            is continuously compared with the value of the P20 counter, if the counter values
                            vary too much, the motion is aborted with an error message. 0 = counter,
                            1 = counter+SFI.""")


    def get_encoder_SFI_tolerance(self):       
        resp = self.execute('P37R')
        return float(resp)
  
    def set_encoder_SFI_tolerance(self, value):
        self.execute('P37S{:f}'.format(value))

    encoder_SFI_tolerance = Number(bounds=(0, None), URL_path='/encoder/sfi-tolerance', db_persist=True,
                                fget=get_encoder_SFI_tolerance, fset=set_encoder_SFI_tolerance,
                                doc="""Encoder tolerance for SFI, Enter tolerance value for SFI evaluation. 
                                Check documentation, Default = 0"""   
                            )

   
    def get_encoder_preferential_direction(self):
        resp = self.execute('P38R')
        return int(resp)
   
    def set_encoder_preferential_direction(self, value):
        self.execute('P38S{}'.format(value))

    encoder_preferential_direction = Integer(bounds=(0, 1), URL_path='/encoder/preferential-direction', 
                                        fget=get_encoder_preferential_direction, fset=set_encoder_preferential_direction,
                                        doc= """Encoder preferential direction of rotation, 0 = + (positive), 1 = – (negative),
                                                Default = 0""")


    def get_encoder_conversion(self):
        resp = self.execute('P39R')
        return float(resp)

    def set_encoder_conversion(self, value):
        self.execute('P39S{:f}'.format(value))

    encoder_conversion =  Number(bounds=(0, None), inclusive_bounds=(False, True), URL_path='/encoder/conversion',
                                fget=get_encoder_conversion, fset=set_encoder_conversion,
                                doc="""Encoder conversion factor, 1 increment corresponds to ...units, Conversion = Thread/Enc-Steps-per-rev
                                    Default = 1"""
                                )



    def get_stop_current(self):
        return float(self.execute('P40R'))*0.01

    def set_stop_current(self, value):
        stopcurr = int(value * 100) #sent to controller in 0.01 A steps
        self.execute('P40S{}'.format(stopcurr))

    stop_current = Number(bounds=(0, 2.5), URL_path='/stop-current', db_persist=True, 
                        fget=get_stop_current, fset=set_stop_current, doc="Stop current in [A]")
    

    run_current = Number(bounds=(0, 2.5), URL_path='/run-current', db_persist=True, 
                        doc="Run current in [A]")

    run_current.getter
    def get_run_current(self):
        resp = self.execute('P41R')
        return float(resp)*0.01

    run_current.setter
    def set_run_current(self, value):
        runcurr = int(value * 100) #sent to controller in 0.01 A steps
        self.execute('P41S{}'.format(runcurr))

    
    def get_boost_current(self) -> float:
        resp = self.execute('P42R')
        return float(resp)*0.01

    def set_boost_current(self, value : float) -> None:
        boostcurr = int(value * 100) # sent to controller in 0.01 A steps
        self.execute('P42S{}'.format(boostcurr))

    boost_current = Number(bounds=(0, 2.5), URL_path='/boost-current', db_persist=True,
                    fget=get_boost_current, fset=set_boost_current, doc= "Boost current in [A]")


    def get_runcurrent_hold_time(self) -> int:
        return int(self.execute('P43R'))

    def set_runcurrent_hold_time(self, value : int) -> None:
        self.execute('P43S{}'.format(value))

    runcurrent_hold_time = Integer(bounds=(0, None), inclusive_bounds=(False, True), URL_path='/run-current/hold-time',
                                db_persist=True, fget=get_runcurrent_hold_time, fset=set_runcurrent_hold_time,
                                doc= """Current hold time in msec after positioning.
                                Caution! Labeled differently in manuals, sometimes
                                "Stoppstromüberhöhungszeit" (stop current boost time) and sometimes
                                "Laufstromüberhöhungszeit" (run current boost time).
                                After finishing positioning, hold at "RunCurrent" for X msec before
                                switching to "StopCurrent". Default = 20.
                                """
                            )


    def get_control_pulse_origin(self) -> int:
        return int(self.execute('P44R'))

    def set_control_pulse_origin(self, value : int) -> None:
        self.execute('P44S{}'.format(value))

    control_pulse_origin = Selector(default=0, objects=[0, 1, 2, 3, 4, 5], URL_path='/control-pulse/origin', db_persist=True,
                                    fget=get_control_pulse_origin, fset=set_control_pulse_origin,
                                    doc="""For I4XM01 only, Origin of the control pulses of the axis,
                                    0: 1:1 (Input = Output), 1 : from X, 2 : from Y, 3 : from Z, 4 : from U,
                                    5 : from external""" 
                                ) 


    def get_step_resolution(self) -> int:
        return StepResolutions(int(self.execute('P45R')))

    def set_step_resolution(self, value : int) -> None:
        self.execute('P45S{}'.format(value))

    step_resolution = Selector(default=0, objects=[resolution for resolution in range(14)], db_persist=True, URL_path='/step-resolution',
                               fget=get_step_resolution, fset=set_step_resolution,
                               doc="""Microstepping step resolution. 0 = 1/1 step,        
                                1 = 1/2 step, 2 = 1/2.5 step, 3 = 1/4 step, 4 = 1/5 step, 7 = 1/16 step, 5 = 1/8 step,
                                8 = 1/20 step, 9 = 1/32 step, 6 = 1/10 step, 10 = 1/64 step, 11 = 1/128 step, 12 = 1/256 step, 
                                13 = 1/512 step (e.g. APS01) Important: for I1AM: step resolution from 1/1 to 1/128 step, 
                                this setting only applies to the INTERNAL power stages or power stages, which are connected via a bus.
                                """
                            )

    #P46 not used
    #P47 not used
    #P48 not used

    power_stage_temperature = Number(readonly=True, URL_path='/power-stage-temperature', 
                                doc="Power stage temperature (not motor temperature!) in degrees celcius.")
    
    @power_stage_temperature.getter
    def PowerStageTemperature(self):
        return int(self.execute('P49R'))/10.0


    def get_control_divider(self):
        return int(self.execute('P50R'))

    def set_control_divider(self, value):
        self.execute('P50S{}'.format(value))

    control_divider = Selector(default=0, objects=[0, 1, 2, 3, 4, 5], db_persist=True, URL_path='/control-pulse/divider',
                        fget=get_control_divider, fset=set_control_divider,
                        doc="""only for I4XM01! Divider for Control pulses, Controlpulses_Output = 1/(n+1) * Controlpulses_Input
                        0 : 1/(0+1)=1, 1: 1/(1+1)= 1/2, 2: 1/(2+1) =1/3, 3: 1/(3+1)=1/4, 4: 1/(4+1)=1/5, 5: 1/(5+1)=1/6
                        Default = 0"""
                    )


    def get_stepping_pulse_width(self):      
        resp = self.execute('P51R')
        return int(resp)

    def set_stepping_pulse_width(self, value):
        self.execute('P51S{}'.format(value))

    pulse_width = Integer(bounds=(0, 255), URL_path='/control-pulse/width',
                        fget=get_stepping_pulse_width, fset=set_stepping_pulse_width,
                        doc="""only for I4XM01! Pulse width: (n+1)*100 ns, n: 0...255, e.g. n=19: (19+1)*100 ns=2000 ns= 2µs,
                                -> F max =1/(2*2 µs)=250 kHz""")  

    # P52 internally used for trigger position

    def get_power_stage_monitoring(self) -> bool:
        return bool(self.execute('P53R'))

    def set_power_stage_monitoring(self, value : bool) -> None:
        self.execute('P53S{}'.format(value))

    power_stage_monitoring = Boolean(URL_path='/power-stage-monitoring',
                                fget=get_power_stage_monitoring, fset=set_power_stage_monitoring,
                                doc="Power stage monitoring, 0 = off, 1 = on (default)" 
                            )

    motor_temperature = Number(readonly=True, URL_path='/motor-temperature', allow_None=True, # when not supported
                            doc="""Motor temperature in degrees celcius. Not supported by all modules/motors!
                                If temperature module not existent, returns NaN. If temperature over/underrange, 
                                returns plus/minus Infinity.""" )
    
    @motor_temperature.getter
    def get_motor_temperature(self) -> float:
        resp = self.execute('P54R')
        resp = int(resp)
        if resp == -999999:
            temp = float('nan')
        elif resp == -9999:
            temp = float('-inf')
        elif resp == 9999:
            temp = float('inf')
        else:
            temp = resp/10.0 #temp. read out in 1/10degC steps
        return temp


    def get_motor_warning_temperature(self) -> float:
        return float(self.execute('P55R'))/10.0
        
    def set_motor_warning_temperature(self, value : float) -> None:
        self.execute('P55S{}'.format(value*10))

    motor_warning_temperature = Number(bounds=(30, None), step=0.1, db_persist=True, URL_path='/motor-temperature/warning',
                                    fget=get_motor_warning_temperature, fset=set_motor_warning_temperature,
                                    doc="""Temperature (in degree Celcius) above which a warning occurs.
                                        If the motor warmed up to a defined temperature value,
                                        a warning occurs. We recommend to operate the motor
                                        only when it is cooled again.""" 
                                    )


    def get_motor_shutoff_temperature(self) -> float:
        return float(self.execute('P56R'))/10.0
     
    def set_motor_shutoff_temperature(self, value : float) -> None:
        self.execute('P56S{}'.format(value*10))

    motor_shutoff_temperature = Number(bounds=(40, None), step=0.1, db_persist=True, URL_path='/motor-temperature/shut-off',
                                    fget=get_motor_shutoff_temperature, fset=set_motor_shutoff_temperature,
                                    doc="""Temperature (in degree Celcius) above which the axis shuts down.
                                        If the motor warmed up to a defined temperature value,
                                        the controller switches off and the power stage must be
                                        reset.""" 
                                    )


    def get_resolver_voltage(self) -> float:
        return float(self.execute('P57R'))
       
    def set_resolver_voltage(self, value : float) -> None:
        self.execute('P57S{}'.format(value))

    resolver_voltage = Number(bounds=(3, 10), crop_to_bounds=True, URL_path='/resolver-voltage', 
                            fget=get_resolver_voltage, fset=set_resolver_voltage, doc="n=3...10V")
   

    def get_resolver_ratio(self):
        resp = self.execute('P58R')
        return int(resp)

    def set_resolver_ratio(self, value):
        self.execute('P58S{}'.format(value))

    resolver_ratio = Selector(default=0, objects=[0, 1, 2, 3, 4], URL_path='/resolver-ratio', db_persist=True, 
                            fget=get_resolver_ratio, fset=set_resolver_ratio,
                            doc="""Resolver ratio (ratio of primary to secondary winding), 0=1/8, 1=1/4,
                                2=1/2, 3=1, 4=2""")

    #********Status flags *****************   
    is_referenced = Boolean(fget=lambda self: AxStatusBits.Referenced in self.AxStatus, 
                                    readonly=True, URL_path='/referenced', 
                                    doc="is the motor currently referenced?" )
     
    is_in_position = Boolean(fget=lambda self: AxStatusBits.In_Position in self.AxStatus,
                                    readonly=True, URL_path='/positioned',
                                    doc="is the motor in last commanded position?")
     
    is_in_hard_negative_limit = Boolean(fget=lambda self: AxStatusBits.Limit_Neg in self.AxStatus,
                                    readonly=True, URL_path='/positioned/hardware-negative', 
                                    doc="is the motor in negative limit?") 
     
    is_in_hard_positive_limit = Boolean(fget=lambda self: AxStatusBits.Limit_Pos in self.AxStatus,
                                    readonly=True, URL_path='/positioned/hardware-positive', 
                                    doc="is the motor in negative limit?") 

    is_in_soft_negative_limit = Boolean(fget=lambda self: AxStatusBits.Softlim_Neg in self.AxStatus,
                                    readonly=True, URL_path='/positioned/software-negative',
                                    doc="is the motor in software negative limit?")

    is_in_soft_positive_limit = Boolean(fget=lambda self: AxStatusBits.Softlim_Pos in self.AxStatus,
                                    readonly=True, URL_path='/positioned/software-positive',
                                    doc="is the motor in software positive limit?")

    is_moving = Boolean(fget=lambda self: (self.execute('!=H') == 'E'), observable=True,
                        readonly=True, URL_path='/moving', doc="is the motor moving?")
      
    @property
    def AxStatus(self):
        resp = self.directcmd('SE{}.{}'.format(self.drive_ID, self.axis))
        statuslong = int(resp)
        stat = AxStatusBits(statuslong)
        return stat # as IntFlag enum, so can easily be used as dword or single flags read out

    @property
    def AxStatusList(self):
        """Instead of an enum, return all currently applied status
        flags as list of strings for convenience."""
        stat = self.AxStatus
        statlist = [flag.name for flag in AxStatusBits if flag in stat]
        return statlist
    
    # *********Position Monitoring*****************
    async def monitor_position(self):
        # at some point this can become an on-entry method for the moving state of state machine
        update_history = True
        self._position_history = PositionHistory()
        while True: 
            try:
                position = self.position
                self.logger.debug("polling position, got value - " + str(position)) # read to trigger change event
                if self.is_moving:
                    await asyncio.sleep(0.1)
                    update_history = True
                elif update_history:
                    current_time = datetime.datetime.now()
                    self._position_history.append(position, current_time)
                    self._last_known_position = position
                    update_history = False
                else:
                    await asyncio.sleep(0.5)
            except Exception as ex:
                self.logger.error("Error in polling position: " + str(ex))
      

    position_history = ClassSelector(readonly=True, URL_path='/position/history', doc="history of positions",
                                class_=PositionHistory, default=None, allow_None=True, fget = lambda self: self._position_history)
    
    last_known_position = Number(URL_path='/position/last-known', db_persist=True, readonly=True,
                                fget=lambda self : self._last_known_position, 
                                doc="Last known position of the stage, updated after movement completion. Survives reboot at the server level instead of device level.")
    
    @action(URL_path='/position/override', input_schema=define_position_schema)
    def define_position(self, position : typing.Optional[float] = None):
        """overwrite current position without moving axis"""
        if position:
            self.position_register = position
        else:
            self.position_register = self._last_known_position

    # ********Actions*****************
    @action(URL_path='/motion/stop')
    def stop_move(self):
        """Normal stop employing usual start stop ramp frequency, refer start_stop_frequency property value."""
        self.execute('S')

    @action(URL_path='/motion/emergency-stop')
    def emergency_stop_move(self):
        """
        Emergency stop which uses harder deceleration ramp, refer emergency_stop_ramp property value.
        Use only in case of emergencies. 
        """
        self.execute("SN")

    @action(URL_path='/motion/relative', input_schema=move_relative_schema)
    def move_relative(self, offset : float) -> None:
        """
        Move relative to current position in either direction by offset value.
        Offset can be positive or negative based on direction of referencing (whether you did ref- or ref+).
        """
        self.execute('{:+f}'.format(offset))
        
    @action(URL_path='/reference/negative', http_method=HTTP_METHODS.POST)
    def reference_negative(self):
        """Reference to negative limit switch, setting position as 0"""
        self.execute('R-')
        
    @action(URL_path='/reference/positive', http_method=HTTP_METHODS.POST)
    def reference_positive(self):
        """Reference to positive limit switch, setting position as 0"""
        self.execute('R+')
        
    @action(URL_path='/motion/limit-neg')
    def goto_limit_neg(self):
        """Move to negative limit switch"""
        if not self.movement_type > 0:
            raise RuntimeError('Not supported for rotational axis (ignoring limits)')
        self.execute('L-') #negative freerun until hits limit
      
    @action(URL_path='/motion/limit-pos')
    def goto_limit_pos(self):
        """Move to positive limit switch"""
        if not self.movement_type > 0:
            raise Exception('Not supported for rotational axis (ignoring limits)')
        self.execute('L+') #positive freerun until hits limit
       
    @action(URL_path='/motion/forward')
    def run_forward(self):
        """
        Move towards forward direction until asked to stop. Unlike goto_lim_<direction>, convention is referencing dependent.
        Danger! Use cautiously, if no limit switches and if connection is lost, no way to stop!
        """
        self.execute('L+')
       
    @action(URL_path='/motion/backward')
    def run_backward(self):
        """
        Move towards backward direction until asked to stop. Unlike goto_lim_<direction>, convention is referencing dependent.
        Danger! Use cautiously, if no limit switches and if connection is lost, no way to stop!
        """
        self.execute('L-')
        
    @action(URL_path='/motion/single-step-forward')
    def step_forward(self):
        """Move one step forward in the given unit of movement"""
        if self.position_units > 1:
            raise Exception('Not supported if units are not steps.')
        self.move_relative(+1)
        
    @action(URL_path='/motion/single-step-backward')
    def step_backward(self):
        """Move one step backward in the given unit of movement"""
        if self.position_units > 1:
            raise Exception('Not supported if units are not steps.')
        self.move_relative(-1)
    
    @action(URL_path='/reset-hardware-status')
    def reset_status(self):
        """Reset hardware status, clears all error flags"""
        self.directcmd('SEC{}.{}'.format(self.drive_ID, self.axis))
