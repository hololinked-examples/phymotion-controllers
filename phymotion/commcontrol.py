# -*- coding: utf-8 -*-
"""
Created on Thu Dec 14 19:04:49 2017

Based on https://github.com/datenwolf/PyPhytron
Adapted for phyMotion Command set

Tested for Py3, may need slight mods for py2.7
20190419 - removed backward compatible names, use only new variables in the future
20190429 - added some IntEnum and IntFlag attributes, These should be compatible with types expecting 
        or providing Int, or at least it should be easy to convert. Tested OK.

@author: Leonard Doyle
"""

# use _ on all variables not to be exposed publicly
import serial as _serial
from serial import SerialException #to expose to public
import threading as _threading
#import sys as _sys

from .constants import PyPhytronException

_STX = b'\x02' #start of telegram
_ETX = b'\x03' #end of telegram
_SEP = b':'    #separator
_ACK = b'\x06' #message valid, acknowledge
_NAK = b'\x15' #unknown or invalid command or checksum error



def _checksum(data):
    """Phymotion communication contains a checksum by XOR-summation of all
    command bytes, including separator: e.g. "0IMR:" -> 92 = 0x5C."""
    if type(data) is str:
        data = bytes(data, 'utf-8')
    chksm = 0
    for d in data:
        chksm = chksm ^ d #ord(d) ord for str type in Py2.7
    return chksm

def _maketelegram(data):
    if type(data) is str:
        data = bytes(data, 'utf-8')
    addr = 0 # always 0 unless on RS485
    addrStr = bytes(str(addr), 'utf-8')
    cmd = addrStr + data + _SEP #checksum calulated incl. separator
    chksum = (b'%02X' % _checksum(cmd))
    #chksum = b'XX'          #dummy XX is accepted by phymotion as valid
    tosend = _STX + cmd + chksum + _ETX
    return tosend

def _decodemsg(buf):
    buf = buf[:-1] #strip trailing _ETX

    isAck = (buf[0] == _ACK[0]) #convert to true or false
    isNak = (buf[0] == _NAK[0]) #convert to true or false
    if not isAck and not isNak:
        raise Exception('Unexpected reply: '+str(buf))

    #    if _sys.version_info >= (3,): #actually not necessary, just return bytes
    #        buf = buf.decode('utf-8') #str after here

    data, chksm = buf.split(_SEP)
    chksm = int(chksm, 0x10)
    expected_chksm = _checksum(data + _SEP)
    data = data[1:] #strip ACK
    if expected_chksm != chksm:
        raise PyPhytronException('Wrong checksum %s != %s' % (expected_chksm, chksm))
    return isAck, data

class CommControlTemplate:
    """Define the common minimum interface that CommControl needs to expose to work
    successfully with the Axis class. Specific implemenations will use either
    pySerial for USB/ Network communication or e.g. Tango commands. In Python
    no inheritance is necessary as only the plain text command names need to
    match, but this class might nevertheless help to clarify the common interface."""
    def __init__(self):
        pass

    def execute(self, cmd):
        """Execute a direct command on the Phymotion. Return result as string
        or throw exception if communication error, invalid command etc."""
        ##Override in subclass to change communication method.
        return ''

    def executeOnAxis(self, moduleID, axisChannel, cmdstr):
        """Execute a command for specific module and axis (usually axis 1 on module)
        and return result as string."""
        ##No need to override in subclass since referring to execute()
        cmd = '{}.{}{}'.format(moduleID, axisChannel, cmdstr)
        return self.execute(cmd)

    def ping(self):
        return self.execute('S') #will return no of axes+ " IO", e.g. "8 IO"

    def ctrlSaveParams(self):
        """Save all current parameters for all axes on controller to persistent
        EEPROM. This is necessary to save most parameters across a reboot.
        Normally, they will be reset to the previous value on power cut."""
        self.execute('SA')


class CommControl(CommControlTemplate):
    def __init__(self, url, baudrate = 115200, portno = 22222, do_not_open=False):
        self._rlock = _threading.RLock()

        if (url.lower().startswith('com') or
            url.lower().startswith('/dev/')):
            #local USB-serial mode
            url = url
        else:
            #network socket mode (TCPIP)
            url = 'socket://' + url + ':{}'.format(portno) #accepts port as str or int

        self._url = url #save for reference
        self._conn = _serial.serial_for_url(url, do_not_open=do_not_open) #defer conn.open() to later
        self._conn.baudrate = baudrate #all these are ignored in socket mode
        self._conn.parity = _serial.PARITY_NONE
        self._conn.rtscts = False
        self._conn.dsrdtr = False
        self._conn.xonxoff = False
        self._conn.timeout = 1
        self._conn.bytesize = 8
        self._conn.stopbits = 1

    def connect(self):
        self._conn.open()

    def close(self):
        self._conn.close()

    def _get_Url(self):
        return self._url

    Url = property(_get_Url) #make read-only property

    def _get_IsConnected(self):
        return self._conn.is_open

    isConnected = property(_get_IsConnected)

    def _get_timeout(self):
        return self._conn.timeout

    def _set_timeout(self, timeout):
        self._conn.timeout = timeout

    timeout = property(_get_timeout, _set_timeout)

    def execute(self, cmdstr):
        """New behaviour on NAK: it does not return two variables (isAck, data)
        but only data and raises error if response is NAK"""
        isAck = False
        recv_data = b''
        with self._rlock:
#            if not self._conn.is_open:
#                self._conn.open() #attempt connect
            self._conn.flushInput() #get rid of any half read data from before
            self._send(cmdstr)
            isAck, recv_data = self._recv()
            #if not recv_data:
            #    recv_data = b'<ACK>' if isAck else b'<NAK>'
            #return NAK is convenient, but actually error is more instructive:
            if not isAck:
                raise PyPhytronException('Invalid command (NAK response): ' + cmdstr)
        return recv_data.decode('utf-8') #empty string if no answer expected for this command

    def _send(self, cmdstr):
        telegram = _maketelegram(cmdstr)
        self._conn.write(telegram)
        self._conn.flush()


    def _recv(self):
        """After sending a telegram, use this to interpret reply."""
        buf = b''

        c = None
        while c != _STX:
            c = self._conn.read(1)
            if not c:
                raise _serial.SerialTimeoutException()

        c = None
        while c != _ETX:
            c = self._conn.read(1)
            if not c:
                raise _serial.SerialTimeoutException()
            buf += bytes(c)

        #message complete
        isAck, data = _decodemsg(buf)

        return isAck, data


    def ctrlSaveParams(self):
        """Override parent implementation to increase timeout."""
        timeout = self._conn.timeout
        self._conn.timeout = 5 #SA takes a long time
        try:
            super().ctrlSaveParams()
        finally:
            self._conn.timeout = timeout

