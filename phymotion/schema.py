import typing, datetime
from dataclasses import dataclass
from hololinked.server.td import JSONSchema

move_relative_schema = {
    "type": "object",
    "properties": {
        "offset" : {
            "type": "number"
        }
    },
    "required": ["offset"],
    "additionalProperties": False
}


define_position_schema = {  
    "type": "object",
    "properties": {
        "position" : {
            "type": "number"
        }
    },
    "required": [],
    "additionalProperties": False
}


@dataclass 
class PositionHistory:
    positions: typing.List[float]
    timestamp: typing.List[str]

    def __init__(self):
        self.positions = []
        self.timestamp = []

    def json(self):
        return {
            "positions": self.positions,
            "timestamp": self.timestamp
        }
    
    def append(self, value : float, timestamp : datetime.datetime):
        self.positions.append(value)
        if isinstance(timestamp, datetime.datetime):
            self.timestamp.append(timestamp.isoformat())
        else:
            self.timestamp.append(timestamp)

    schema = {
        "type": "object",
        "properties": {
            "positions": {
                "type": "array",
                "items": {
                    "type": "number"
                }
            },
            "timestamp": {
                "type": "array",
                "items": {
                    "type": "string",
                    "format": "date-time"
                }
            }
        },
        "required": ["positions", "timestamp"],
        "additionalProperties" : False
    }

JSONSchema.register_type_replacement(PositionHistory, "object", PositionHistory.schema)