# -*- coding: utf-8 -*-
"""
Constants, enum definitions etc. for the Phymotion controller

Created on Thu Sep 17 10:22:49 2020

@author: Leonard.Doyle
"""

#use _ on all variables not to be exposed publicly

from enum import IntEnum as _IntEnum
from enum import IntFlag as _IntFlag


class PyPhytronException(Exception):
    """Exceptions specific to the logic of PyPhytron. E.g. if a command is
    invalid. Does not try to wrap or replace SerialException, which shows
    more low-level problem."""
    pass


class MovementTypes(_IntEnum):
    """Rotational= limits ignored except for referencing"""
    Rotational = 0
    HardwareLimits = 1
    SoftwareLimits = 2
    HardAndSoftLimits = 3

class PositionUnitTypes(_IntEnum):
    Steps = 1
    Length_mm = 2
    Length_inch = 3
    Angle_deg = 4

class LimitSwitchTypes(_IntEnum):
    """NC = normally closed, activates switch when circuit open (LO signal).
    NO = normally open, activates switch when circuit closed (HI signal)."""
    NegNC_MidNC_PosNC = 0
    NegNC_MidNC_PosNO = 1
    NegNO_MidNC_PosNC = 2
    NegNO_MidNC_PosNO = 3
    NegNC_MidNO_PosNC = 4
    NegNC_MidNO_PosNO = 5
    NegNO_MidNO_PosNC = 6
    NegNO_MidNO_PosNO = 7

class StepResolutions(_IntEnum):
    """I1AM: only from 1/1 to 1/128 supported. Slightly weird naming since
    variable names must start with a letter and not contain dots etc."""
    OneOver1_Full = 0
    OneOver2_Half = 1
    OneOver2p5 = 2          #1/2.5
    OneOver4_Quarter = 3
    OneOver5 = 4
    OneOver8 = 5
    OneOver10 = 6
    OneOver16 = 7
    OneOver20 = 8
    OneOver32 = 9
    OneOver64 = 10
    OneOver128 = 11
    OneOver256 = 12
    OneOver512 = 13

    def __init__(self, i):
        self.divider = {
             0: 1,
             1: 0.5,
             2: 1/2.5,
             3 : 1/4,
             4 : 1/5,
             5 : 1/8,
             6 : 1/10,
             7 : 1/16,
             8 : 1/20,
             9 : 1/32,
             10 : 1/64,
             11 : 1/128,
             12 : 1/256,
             13 : 1/512,
             }[i]

class AxStatusBits(_IntFlag):
    """Usage:
        stat = axis.AxStatus #returns AxStatusBits enum
        isRef = AxStatusBits.Referenced in stat #ask if bit is set in 'stat' (recommended)
        or by
        isRef = bool(stat & AxStatusBits.Referenced) #bitwise operation
        or by
        isRef = bool(stat & stat.Referenced) #shorthand but not sure if 'Pythonic'/ nice way
        """
    Busy =              0x1
    Invalid_Cmd =       0x2
    Await_Sync =        0x4
    Referenced =        0x8
    Limit_Pos =         0x10
    Limit_Neg =         0x20
    Limit_Mid =         0x40
    Softlim_Pos =       0x80
    Softlim_Neg =       0x100
    PowerStage_Ready =       0x200 #Caution: german manual says "Endstufe bereit", english "power stage busy", Ready seems correct
    Ramping =           0x400
    Err_Internal =      0x800
    Err_Limits =        0x1000
    Err_PowerStage =    0x2000
    Err_SFI =           0x4000
    Err_ENDAT =         0x8000
    Moving =            0x10000
    Ax_in_Waittime =    0x20000
    Ax_in_Stopboosttime = 0x40000
    In_Position =       0x80000
    APS_ready =         0x100000
    Positioning_Mode =  0x200000
    Freerun_Mode =      0x400000
    Multi_F_Mode =      0x800000
    Sync_Allowed =      0x1000000