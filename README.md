# PyPhymotion

Python module to interface Phytron phyMotion controllers. Provides HTTP support with ``hololinked`` server side.
Still in development, does not work, not suitable for usage. 

For the original code visit [here](https://gitlab.lrz.de/cala-public/packages/pyphymotion)

# Structure

These two objects are of importance -

* ``Axis`` - controls a single IO module/axis/motor controller from the entire rack
* ``CommControl`` - serial command handler

Therefore to control just a single axis/module, use 

```python
comm = CommControl(url="COM3")
axis_1 = Axis(comm, moduleID=1) 
```

To control more than one axis/module, one needs to share the ``CommControl`` object

```python
comm = CommControl(url="COM3")
axis_1 = Axis(comm, moduleID=1) 
axis_2 = Axis(comm, moduleID=2)
axis_1.ReferenceNeg()
axis_2.MoveRelative(5)

