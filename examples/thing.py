import logging, ssl, os 
from phymotion import Axis, CommControl
from hololinked.server import HTTPServer
from multiprocessing import Process

def start_https_server():
    ssl_context = ssl.SSLContext(protocol = ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain(f'assets{os.sep}security{os.sep}certificate.pem',
                        keyfile=f'assets{os.sep}security{os.sep}key.pem')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_3

    H = HTTPServer(things=['linear-stage'], port=9000, 
                log_level=logging.DEBUG,  ssl_context=ssl_context)
    H.listen()


if __name__ == '__main__':
    P = Process(target=start_https_server)
    P.start()
    
    comm = CommControl("COM8")
    axis = Axis(instance_name='linear-stage', comm_handle=comm, drive_ID=1) # example, axis 3 might be tilt motor
    axis.run()
