# -*- coding: utf-8 -*-
"""
Created on Wed Aug 21 02:06:45 2019

To manually compile form in IPython console:
!pyuic5 ShotPanel.ui -o ShotPanelUI.py
(! tells IPython to run native system command, e.g. "!ls" or "!dir")

@author: Leonard.Doyle
"""

import os

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.uic import compileUi

def running_in_ipython():
    """Workaround: if running in IPython, do not use sys.exit() to close kernel
    but only close GUI and keep python running."""
    try:
        __IPYTHON__
        return True
    except NameError:
        return False

def compileUiIfNecessary(uifile, pyfile=None):
    """Check if the given UI file is newer than the corresponding .py
    file (same name with _ui appended if None specified) and try to
    recompile in this case.
    No error is raised if this fails, only a warning is printed since
    this is not catastrophic but only potentially means we are using an
    outdated UI."""
    #only tested in same directory
    if not pyfile:
        pyfile = os.path.splitext(uifile)[0] + '_ui.py'
    
    try:
        """
        If only .ui file exists, .py file is compiled
        If both .ui and .py file exist, the .py is recompiled if it is older
        than the .ui file
        If only the .py file exists, no comparison with .ui file is tried.
        """
        if not os.path.exists(pyfile):
            if not os.path.exists(uifile):
                raise FileNotFoundError(
                        'Error converting {}, file not found'.format(uifile))
                return
            print('>>Compiling {} from {}'.format(pyfile, uifile))
            with open(pyfile,'w',encoding='utf8') as pyfile:
                compileUi(uifile,pyfile)
        elif os.path.exists(uifile): #both exist
            if (os.path.getmtime(uifile) >
                os.path.getmtime(pyfile)):
                print('>>{} older than {}, recompiling'.format(
                        pyfile, uifile))
                with open(pyfile,'w',encoding='utf8') as pyfile:
                    compileUi(uifile,pyfile)
    except PermissionError:
        print('>>Warning! Unable to compile {} from {} due to'+
              ' permissions.'.format(pyfile, uifile))

