# pyMotorPanel

Qt GUI client to interface with PhymotionMotor devices. Simple interface for controlling position, movement and homing.  Additional settings and direct control for manual command entering.

Device Settings
- Position Units
- Conversion
- Run Frequency
- Acceleration
- Limit Switch Type
- Movement Type
- Softlimit + 
- Softlimit -
- Play Compensation
- Run Current
- Stop Current
- Step Resolution

Movement Control
- Ref +/-
- Rel Move +/-
- Lim +/-
- Absolute +/-

Status
- module and channel
- whether referenced
- whether in limits
- whether position override

# Project Responsible

Vignesh Vaidyanathan

# Preview

<table>
  <tr>
    <td>
      <img src="assets/full-preview.png" alt="First Image" style="width:100%;">
    </td>
    <td>
      <img src="assets/half-preview-1.png" alt="Second Image" style="width:100%;">
      <br>
      <img src="assets/half-preview-2.png" alt="Third Image" style="width:100%;">
    </td>
  </tr>
</table>




