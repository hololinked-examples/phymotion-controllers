# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 16:11:58 2017

To compile form:
!pyuic5 MotorPanel.ui -o MotorPanel_ui.py

@author: lenny
"""

import sys
from PyQt5.QtWidgets import (QApplication, QShortcut, QMessageBox, QLabel, 
                                QVBoxLayout, QScrollArea, QFrame, QGroupBox)
from PyQt5.QtCore import QTimer, pyqtSlot, Qt
from PyQt5 import QtGui

from hololinked.client import ObjectProxy
import datetime

from base_pyqt_gui_panel import BasePanel
from base_pyqt_gui_panel.UIHelpers import *
from base_pyqt_gui_panel.ExceptionHandlers import *
from phymotion import Axis

try:
    compileUiIfNecessary('examples\pyqt_example\MotorPanel.ui')
except FileNotFoundError:
    compileUiIfNecessary('MotorPanel.ui')
from MotorPanel_ui import Ui_MainWindow

_styleOnGreen = 'background-color: rgb(85, 170, 0);'
_styleOffGrey = ''
_styleOnYellow = 'background-color: rgb(255, 255, 0);'
_styleOnOrange = 'background-color: rgb(255, 127, 0);'

requestProcessID('pyMotorPanel')



class MotorPanel(BasePanel, Ui_MainWindow):
    
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        
        # Device
        self.device = None # currently connected device
        self.controldevice = None # controlling device of currently connected device
        # following avoids generating multiple error boxes
        self._has_fault = False

        # Fill & connect the UI elements
        self.init_UI()
        # init logic
        self.loadDeviceList()
        self.refresh()
        # prepare GUI
        self.btnInfo.setChecked(True)
        self.showMorePanels(True, False) #hide Settings, Direct Control
        self.show()


    def init_UI(self):
        self._origMaxSize = self.maximumSize() #save for later
        self.setWindowIcon(QtGui.QIcon('assets/pyMotorPanel.png'))

        # Scroll area for settings
        self.CentralLayout.removeWidget(self.grpSettings)
        self.scrollAreaTitleGroupBox       = QGroupBox(self.centralwidget) 
        self.scrollAreaTitleGroupBoxLayout = QVBoxLayout(self.scrollAreaTitleGroupBox)
        self.scrollAreaTitleGroupBoxLayout.setContentsMargins(0,0,0,2)
        self.scrollAreaTitleGroupBox.setTitle('Settings')
        self.scrollAreaSettings = QScrollArea(self.centralwidget)
        self.scrollAreaSettings.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollAreaSettings.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollAreaSettings.setMinimumHeight(400)
        self.grpSettings.setStyleSheet("QGroupBox { border: 0px; }")
        self.scrollAreaSettings.setWidget(self.grpSettings)
        self.scrollAreaSettings.setFrameShape(QFrame.NoFrame)
        self.scrollAreaSettings.setWidgetResizable(True)
        self.scrollAreaTitleGroupBoxLayout.addWidget(self.scrollAreaSettings)
        self.CentralLayout.addWidget(self.scrollAreaTitleGroupBox)
        
        stopshortcut = QShortcut(self)
        stopshortcut.setKey('F4')
        stopshortcut.activated.connect(self.stop)
        
        self.refreshtimer = QTimer(self)
        self.refreshtimer.timeout.connect(self.refresh)
        self.refreshtimer.setInterval(10000) 
       
        # settings, signals, slots of grpBaseOptions
        self.btnInfo.clicked.connect(self.toggleMorePanels)
        self.btnSettings.clicked.connect(self.toggleMorePanels)
        self.btnRefresh.clicked.connect(self.refresh)
        
        self.btnConnect.clicked.connect(self.connect)
        self.btnDisconnect.clicked.connect(self.disconnect)        
        self.btnStop.clicked.connect(self.stop)
        
        self.grpBaseOptions.setStyleSheet("QGroupBox { border: 0px; }")
        
        # settings, signals, slots of grpControl
        self.lblIsRef.setMargin(7) #pyuic seems to ignore margin, so set
        self.lblSoftLimNeg.setMargin(7) # here manually
        self.lblLimitNeg.setMargin(7) 
        self.lblLimitPos.setMargin(7) 
        self.lblSoftLimPos.setMargin(7)         
        
        self.btnStartRefNeg.clicked.connect(self.startRefNeg)
        self.btnStartRefPos.clicked.connect(self.startRefPos)
        self.btnGoNeg.clicked.connect(self.gotoLimNeg)
        self.btnGoPos.clicked.connect(self.gotoLimPos)
        self.btnPosSet.clicked.connect(self.setPosition)
        self.btnPosRelNeg.clicked.connect(self.setPositionRelNeg)
        self.btnPosRelPos.clicked.connect(self.setPositionRelPos)
        
        # settings, signals, slots of grpSettings
        self.btnPosUnitSet.clicked.connect(self.setPosUnits)
        self.btnConversionSet.clicked.connect(self.setConversion)
        self.btnRunFrequencySet.clicked.connect(self.setRunFrequency)
        self.btnAccelerationSet.clicked.connect(self.setAcceleration)
        self.btnLimitSwitchSet.clicked.connect(self.setLimitType)
        self.btnMoveTypeSet.clicked.connect(self.setMoveType)
        self.btnSoftlimNegSet.clicked.connect(self.setSoftlimNeg)
        self.btnSoftlimPosSet.clicked.connect(self.setSoftlimPos)
        self.btnPlayCompensationSet.clicked.connect(self.setPlayCompensation)
        self.btnRunCurrentSet.clicked.connect(self.setRunCurrent)
        self.btnStopCurrentSet.clicked.connect(self.setStopCurrent)
        self.btnStepResolutionSet.clicked.connect(self.setStepResolution)
        
        # Cancel Wheel Event i.e. no box should increment/decrement value with mouse wheel rotation
        self.txtPosSet.wheelEvent = lambda event: None
        self.txtPosRelStep.wheelEvent = lambda event: None
        self.txtPosUnit.wheelEvent = lambda event: None
        self.txtConversion.wheelEvent = lambda event: None
        self.txtRunFrequency.wheelEvent = lambda event: None
        self.txtAcceleration.wheelEvent = lambda event: None
        self.txtLimitSwitch.wheelEvent = lambda event: None
        self.txtMoveType.wheelEvent = lambda event: None
        self.txtSoftlimPos.wheelEvent = lambda event: None
        self.txtSoftlimNeg.wheelEvent = lambda event: None
        self.txtPlayCompensation.wheelEvent = lambda event:None
        self.txtRunCurrent.wheelEvent = lambda event: None
        self.txtStopCurrent.wheelEvent = lambda event: None
        self.txtStepResolution.wheelEvent = lambda event: None
    
        # Menu 'options'
        self.actionRefreshDevicesList.triggered.connect(self.loadDeviceList)

        # Status bar
        self.statusUpdateSig.connect(self.updateStatus)
        self.txtTemperature = QLabel()
        self.clock = QLabel(datetime.datetime.strftime(datetime.datetime.now(), '%H:%M:%S'))
        self.statusBar().addPermanentWidget(self.txtTemperature)
        self.statusBar().addPermanentWidget(self.clock)


    #*****error management code*****      
    @pyqtSlot(str)
    def updateStatus(self, val):
        """
        show error messages onto status bar for 5 seconds,
        to show tango status on status bar use update_tango_status

        :param val: message text to be shown
        :return:
        """
        self.statusBar().showMessage(val, 10000) 
   
    
    #****UI Code****
    def toggleMorePanels(self):
        self.showMorePanels(self.btnInfo.isChecked(), self.btnSettings.isChecked())
    
    
    def showMorePanels(self, showInfo, showSettings):
        """Toggle the display of additional panels and handle the size
        policies."""
        self.setUpdatesEnabled(False) #reduce flicker on screen during redraw
        self.grpDevice.setVisible(showInfo)
        self.scrollAreaTitleGroupBox.setVisible(showSettings)
        
        oldwidth = self.width()        
        self.adjustSize() # update layout to be minimal size
        self.resize(oldwidth, 0) # still have to shrink it for some reason
         
        self.setUpdatesEnabled(True)


    @property
    def suffix_conversion_decimals(self):
        if hasattr(self, '_suffix_conversion_decimals'):
            return self._suffix_conversion_decimals 
        else:
            # key : (suffix, conversionsuffix, decimals)
            self._suffix_conversion_decimals = {
                1 : ('', '', 0),
                2 : ('mm','mm/stp', 3),
                3 : ('in','in/stp', 3),
                4 : ('°','°/stp',3),
            }
            return self._suffix_conversion_decimals
    

    def refresh(self, suppress_error_box = False):
        """Update all fields to show current status."""   
        self.refreshtimer.stop()    
        isConnected = (self.device != None)
        self.cbxMotor.setEnabled(not isConnected)
        self.btnDisconnect.setEnabled(isConnected)
        self.btnConnect.setEnabled(not isConnected)
        # spare the buttons Refresh, Settings, Direct Control, always useful        
        self.btnStop.setEnabled(isConnected)
        self.grpControl.setEnabled(isConnected)
        self.grpSettings.setEnabled(isConnected)
      
        if isConnected:
            try:
                #units handling
                units = self.device.position_units
                suffix, conversionsuffix, decimals = self.suffix_conversion_decimals.get(units, ('','',2))
                
                self.txtConversion.setSuffix(conversionsuffix)
                self.txtPosGet.setSuffix(suffix)
                self.txtPosSet.setSuffix(suffix)
                self.txtPosRelStep.setSuffix(suffix)
                self.txtSoftlimPos.setSuffix(suffix)
                self.txtSoftlimNeg.setSuffix(suffix)
                self.txtPlayCompensation.setSuffix(suffix)
                
                self.txtPosGet.setDecimals(decimals)
                self.txtPosSet.setDecimals(decimals)
                self.txtPosRelStep.setDecimals(decimals)
                self.txtSoftlimPos.setDecimals(decimals)
                self.txtSoftlimNeg.setDecimals(decimals)
                self.txtPlayCompensation.setDecimals(decimals)
             
                self.txtTemperature.setText("Axis Controller Temp: {:.1f} °C".format(self.device.power_stage_temperature))
                        
                if not self.txtPosUnit.hasFocus():
                    self.txtPosUnit.setValue(self.device.position_units)
                if not self.txtConversion.hasFocus():
                    self.txtConversion.setValue(self.device.conversion_factor)
                if not self.txtRunFrequency.hasFocus():
                    self.txtRunFrequency.setValue(self.device.run_frequency)
                if not self.txtAcceleration.hasFocus():
                    self.txtAcceleration.setValue(self.device.acceleration)
                if not self.txtLimitSwitch.hasFocus():
                    self.txtLimitSwitch.setValue(self.device.limit_switch_type)
                if not self.txtMoveType.hasFocus():
                    self.txtMoveType.setValue(self.device.movement_type)
                if not self.txtSoftlimNeg.hasFocus():
                    self.txtSoftlimNeg.setValue(self.device.soft_limit_min)
                if not self.txtSoftlimPos.hasFocus():
                    self.txtSoftlimPos.setValue(self.device.soft_limit_max)
                if not self.txtPlayCompensation.hasFocus():
                    self.txtPlayCompensation.setValue(self.device.play_compensation)
                if not self.txtRunCurrent.hasFocus():
                    self.txtRunCurrent.setValue(self.device.run_current)
                if not self.txtStopCurrent.hasFocus():
                    self.txtStopCurrent.setValue(self.device.stop_current)
                if not self.txtStepResolution.hasFocus():
                    self.txtStepResolution.setValue(self.device.step_resolution)
                self.lblIsRef.setStyleSheet(_styleOnGreen if
                                        self.device.is_referenced else
                                        _styleOffGrey)
                self.lblLimitNeg.setStyleSheet(_styleOnYellow if
                                            self.device.is_in_hard_negative_limit else
                                            _styleOffGrey)
                self.lblLimitPos.setStyleSheet(_styleOnYellow if
                                            self.device.is_in_hard_positive_limit else
                                            _styleOffGrey)
                self.lblSoftLimNeg.setStyleSheet(_styleOnOrange if
                                            self.device.is_in_soft_negative_limit else
                                            _styleOffGrey)
                self.lblSoftLimPos.setStyleSheet(_styleOnOrange if
                                            self.device.is_in_soft_positive_limit else
                                            _styleOffGrey)
                
                self.txtPosGet.setValue(self.device.position)
            except Exception:
                self._has_fault = True
                self.error(self.refresh, sys.exc_info(), suppress_error_box) 
                suppress_error_box = True
                # refresh timer already stopped at beginning of function and will not be restarted 
                # because has_fault_1 = True. See end of function. 
            else:
                self._has_fault = False
                # refresh time is automatically restarted at the end if there is no fault
               
        else:
            self.lblDevCtrl.setText('')
            self.lblDevModChnl.setText('')
            
            self.lblIsRef.setStyleSheet(_styleOffGrey)
            self.lblLimitNeg.setStyleSheet(_styleOffGrey)
            self.lblLimitPos.setStyleSheet(_styleOffGrey)
            self.txtPosGet.setValue(0.0)
            self.txtPosSet.setValue(0.0)
            self.txtPosRelStep.setValue(100.0)
            self.txtPosUnit.setValue(1)
            self.txtConversion.setValue(0.0)
            self.txtRunFrequency.setValue(0)
            self.txtAcceleration.setValue(0)
            self.txtLimitSwitch.setValue(0)
            self.txtMoveType.setValue(0)
            self.txtSoftlimPos.setValue(0.0)
            self.txtSoftlimNeg.setValue(0.0)
            self.txtPlayCompensation.setValue(0.0)
            self.txtRunCurrent.setValue(0.0)
            self.txtStopCurrent.setValue(0.0)
            self.txtStepResolution.setValue(0)
            self.txtTemperature.setText('')
            
            #units handling
            self.txtConversion.setSuffix('')
            self.txtPosGet.setSuffix('')
            self.txtPosSet.setSuffix('')
            self.txtPosRelStep.setSuffix('')
            self.txtSoftlimPos.setSuffix('')
            self.txtSoftlimNeg.setSuffix('')
            self.txtPlayCompensation.setSuffix('')
            
            self.txtPosGet.setDecimals(2)
            self.txtPosSet.setDecimals(2)
            self.txtPosRelStep.setDecimals(2)
            self.txtSoftlimPos.setDecimals(2)
            self.txtSoftlimNeg.setDecimals(2)
            self.txtPlayCompensation.setDecimals(2)

            self._has_fault = False

            self.statusbar.clearMessage()

        if not self._has_fault:   
            self.refreshtimer.start()


    

    #****Logic Code****
    @handleSlotWithException(refresh=False)
    def loadDeviceList(self):
        """
        load existing devices from tango database. 
        """   
        if self.device is not None:
            # need to store name now for edge case where
            # device is now offline, but still connected
            cur_name = self.cbxMotor.currentText()
        self.cbxMotor.addItems(['linear-stage'])
    

    def connect(self):
        """
        Connect to specified device proxy of stage.
        """
        try:
            self.device = ObjectProxy(instance_name=self.cbxMotor.currentText()) # type: Axis
        except Exception as Exc:
            self.device = None
            self.errorbox(self.connect, sys.exc_info())
        else:
            self.setup()


    @handleMethodWithException
    def setup(self):
        """
        Error message from connecting device must be special and largely quite useful.
        Therefore we separate the connect device logic from logic which reads important information
        to setup UI for the device. 
        """
        # Window related
        self.setWindowTitle(self.device.instance_name + ' - Motor Control')
        # grp Device set some props
        self.grpDevice.setTitle('Device - {}'.format(self.device.instance_name))
        # one-time read attributes
        self.txtPosRelStep.setValue(0.1)
        self.device.subscribe_event('position_change_event', self.update_position)
                
    @handleSlotWithException
    def disconnect(self):
        """
        disconnect from device. 
        """
        self.device = None
        self.setWindowTitle('disconnected - Motor Control')
        self.grpDevice.setTitle('Device - disconnected')
            
    
    #*****Non connection-disconnection related slots - mostly attributes and commands*****
    def update_position(self, value):
        try:
            self.txtPosGet.setValue(value)
        except Exception as ex:
            print(ex)
            
    @handleSlotWithException
    def stop(self):
        self.device.stop_move()

    @handleSlotWithException
    def setPosition(self):
        self.device.position = self.txtPosSet.value()    
        
    @handleSlotWithException
    def startRefNeg(self):
        reply = QMessageBox.question(self,'Reference?','Start referencing to -?',
                                QMessageBox.Ok | QMessageBox.Cancel)
        if reply == QMessageBox.Ok:
            self.device.reference_negative()

    @handleSlotWithException
    def startRefPos(self):
        rep = QMessageBox.question(self,'Reference?','Start referencing to +?',
                                QMessageBox.Ok | QMessageBox.Cancel)
        if rep == QMessageBox.Ok:
            self.device.reference_positive()
            
    @handleSlotWithException
    def gotoLimNeg(self):
        self.device.goto_limit_neg()           
        
    @handleSlotWithException
    def gotoLimPos(self):
        self.device.goto_limit_pos()            
        
    @handleSlotWithException
    def setPositionRelNeg(self):
        self.device.move_relative(-self.txtPosRelStep.value())
        
    @handleSlotWithException
    def setPositionRelPos(self):
        self.device.move_relative(self.txtPosRelStep.value())    
        
    @handleSlotWithException
    def setPosUnits(self):
         self.device.position_units = self.txtPosUnit.value()        
        
    @handleSlotWithException
    def setConversion(self):
        self.device.conversion_factor = self.txtConversion.value()     
        
    @handleSlotWithException
    def setRunFrequency(self):
        self.device.run_frequency = self.txtRunFrequency.value()
            
    @handleSlotWithException
    def setAcceleration(self):
        self.device.acceleration = self.txtAcceleration.value()
            
    @handleSlotWithException
    def setLimitType(self):
        self.device.limit_switch_type = self.txtLimitSwitch.value()
    
    @handleSlotWithException
    def setMoveType(self):
        self.device.movement_type = self.txtMoveType.value()
            
    @handleSlotWithException   
    def setSoftlimPos(self):
        self.device.soft_limit_max = self.txtSoftlimPos.value()
            
    @handleSlotWithException
    def setSoftlimNeg(self):
        self.device.soft_limit_min = self.txtSoftlimNeg.value()
            
    @handleSlotWithException
    def setPlayCompensation(self):
        self.device.play_compensation = self.txtPlayCompensation.value()

    @handleSlotWithException    
    def setRunCurrent(self):
        self.device.run_current = self.txtRunCurrent.value()
        
    @handleSlotWithException
    def setStopCurrent(self):
        self.device.stop_current = self.txtStopCurrent.value()
            
    @handleSlotWithException
    def setStepResolution(self):
        self.device.step_resolution = self.txtStepResolution.value()
         

if __name__ == '__main__':
    app = None
    app = QApplication(sys.argv)
    mt = MotorPanel()
    if(running_in_ipython()):
        app.exec_() #do not exit kernel if in IPython
    else:
        sys.exit(app.exec_())