# -*- coding: utf-8 -*-
"""
Demo file showing the use of the PyPhytron module to interface a Phymotion
via USB or Ethernet.
"""

import time
import traceback # for exception formatting
from phymotion import Axis, CommControl, PositionUnitTypes

    
if __name__ == '__main__':
    """
    First, choose a URL for the connection either via USB (Virtual serial port)
    or Ethernet (IP address).
    """
    connect_url = 'COM9' #choose correct port
    # connect_url = '192.168.0.13' #use this instead for network
    
    comm = None # initialize empty to indicate not yet connected
    
    try:
        """
        To initialize a communication channel, create an instance of CommControl.
        This will be the only handle to communicate with the controller directly,
        shared by all axes.
        """
        comm = CommControl(connect_url)
        
        """
        Next, initialize one or more axes. Pass the comm object as 
        communication channel, the module number (number of IO card in phymotion).
        """
        axis = Axis(instance_name='linear-stage', comm_handle=comm, drive_ID=1) #example, axis 3 might be tilt motor
        """
        The axes will all share a single comm object since only one connection
        line to Phymotion can be established. Each axis object is then a wrapper
        to provide axis specific functions and parameters.
        """
        
        """Most parameters can be read and written as Python attributes like so:"""
        axunits = axis.position_units
        if axunits != PositionUnitTypes.Steps:
            #axis units are not [steps] but something else like [mm] or [rad/deg]
            print('Setting axis to steps units for demo')
            axis.position_units = PositionUnitTypes.Steps
        
        """
        There are several ways to initiate movement on the axis:
        - write to ax.position and set absolute position
        - use ax.move_relative(distance) to go relative
        - use ax.run_forward() for freerun
        - use ax.goto_limit_neg() or ...Pos() to go to end switch without referencing
        - use ax.reference_neg() or ...Pos() to go to end switch and set as new 0
        """
        print('Current position: {}'.format(axis.position))
        axis.move_relative(10)
        while(axis.is_moving):
            """
            When the motor is moving, Position will of course change at every
            read-out until movement finished, so wait here until done.
            """
            time.sleep(0.05)
            print(axis.position)
        print('Finished moving at {}'.format(axis.position))
        
        print('Is on negative limit switch? {}'.format(
                  'Yes' if axis.is_in_hard_negative_limit else 'No'))
        
        """
        (Advanced users)
        On top of the commands implemented, you can directly communicate with
        the controller to send special commands, here are some demonstrations:
        """
        print(axis.execute('P20R')) #read Position register (P20) of axis Tilt (here Module 3)
        print(axis.directcmd('IMR')) #directcmd = axis independent, read MAC address
        
        print(comm.execute('IMR')) # equivalently execute directly on controller, axis independent
        
        """If something goes wrong, generally there will be an exception:"""
        print(comm.execute('SOMECMD')) #will raise NAK (Unknown command, error) exception
    except:
        """
        Always wrap in try/except appropriately
        """
        print(traceback.format_exc())
    
    if comm is not None:
        """Important: Finally, if connection was established, close to ensure
        the connection can be re-established on next run."""
        comm.close() #important! make sure the Serial port/ network socket is closed in the end