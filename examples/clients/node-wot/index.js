// client.js
// Required steps to create a servient for a client
const { Servient } = require("@node-wot/core");
const { HttpsClientFactory } = require("@node-wot/binding-http");

const servient = new Servient();
servient.addClientFactory(new HttpsClientFactory({ allowSelfSigned : true }));


servient.start().then(async(WoT) => {

    const td = await WoT.requestThingDescription("https://localhost:8080/linear-stage/resources/wot-td");
    // Then from here on you can consume the thing
    client = await WoT.consume(td);
    console.log("consumed TD");

    console.log("client started");

    console.log(await (await client.readProperty('position')).value())
    console.log(await (await client.readProperty('run_frequency')).value())
    console.log(await client.readAllProperties())
    if(await (await client.readProperty('is_in_hard_negative_limit')).value()) {
        console.log("going to positive limit")
        console.log(await client.invokeAction('goto_limit_pos'))
    } else {
        console.log("going to negative limit")
        console.log(await client.invokeAction('goto_limit_neg'))
    }
})